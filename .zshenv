# ZSH
if [[ "$OSTYPE" = darwin* ]]; then
  SHORT_HOST=$(scutil --get ComputerName 2>/dev/null) || SHORT_HOST="${HOST/.*/}"
else
  SHORT_HOST="${HOST/.*/}"
fi

export ZDOTDIR="$HOME/.config/zsh"
export ZSH_CACHE_DIR="$ZDOTDIR/cache"
export ZCOMPDUMP="$ZSH_CACHE_DIR/.zcompdump-${SHORT_HOST}"
export SHELL_SESSIONS_DISABLE=1

# EDITOR
export EDITOR=vim
export VISUAL=$EDITOR
export SUDO_EDITOR=$EDITOR

# FZF
if [[ -d "$HOME/.local/src/fzf" ]]; then
  export FZF_BASE="$HOME/.local/src/fzf/"
  export DISABLE_FZF_AUTO_COMPLETION="false"
  export DISABLE_FZF_KEY_BINDINGS="false"
fi

# LESS
export PAGER=less
export LESS='--mouse -F -i -R -Q -J -M -W -X -x4 -z-4'
export LESSOPEN="|$HOME/.local/bin/lesspipe.sh %s"

# PYTHON
export PYTHONPATH="$HOME/Projects/python/modules"
export PYTHONUSERBASE="$HOME/.local"

if [[ -n $VIRTUAL_ENV && -e "${VIRTUAL_ENV}/bin/activate" ]]; then
  source "${VIRTUAL_ENV}/bin/activate"
fi

# SSH Keys
if ! [[ ( -v SSH_CONNECTION || -v SSH_TTY ) ]]; then
  if ! ssh-add -q -L >/dev/null; then
    ssh-add --apple-load-keychain
  fi
fi

# YAMLFIX
if [[ -f "$HOME/.config/yamlfix/yamlfix" ]]; then
  source "$HOME/.config/yamlfix/yamlfix"
fi

# vim: ft=zsh ts=2 sts=2 sw=2 sr et
